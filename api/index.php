<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers: Content-type');


//verifica se há uma requisição
if($_REQUEST){

    $url = explode('/', $_REQUEST['url']);

    //rotas disponiveis
    switch($url[0]){

        //lista os ToDo's não concluídos
        case 'listToDo':
            include("./src/repositories/listToDo.php");
        break;

        //lista todos os ToDo's  
        case 'listAllToDo':
            include("./src/repositories/listAllToDo.php");
        break;

        //lista os ToDo's concluídos
        case 'listCheckedToDo':
            include("./src/repositories/listCheckedToDo.php");
        break;

        //adiciona um ToDo novo  
        case 'addToDo':
            include("./src/repositories/addToDo.php");
        break;

        //Atualiza o ToDo  
        case 'updateContent':
            //confirma a existencia do parametro Id para o redirecionamento
            if($url[1]){
                include("./src/repositories/updateContent.php");
             }else
             echo json_encode(array('status' => 404, 'message' => 'Request Invalid'));
        break;

        //Marca o todo como concluído
        case 'checkedToDo':
            //confirma a existencia do parametro Id para o redirecionamento
            if($url[1]){
                include("./src/repositories/updateChecked.php");
             }else
             echo json_encode(array('status' => 404, 'message' => 'Request Invalid'));
        break;

        //deleta o ToDo
        case 'deleteToDo':
            //confirma a existencia do parametro Id para o redirecionamento
            if($url[1]){
                include("./src/repositories/deleteToDo.php");
             }else
             echo json_encode(array('status' => 404, 'message' => 'Request Invalid'));
        break;

        //caso o client faça a requisição de uma rota invalida
        default:
            echo json_encode(array('status' => 404, 'message' => 'Route Invalid'));

    }
}

?>