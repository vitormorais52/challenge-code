import {http} from './config';

export default {

    list:() => {
        return http.get('listToDo');
    },

    listAll:() => {
        return http.get('listAllToDo');
    },

    listChecked:() => {
        return http.get('listCheckedToDo');
    },

    addToDo: (newToDo) => {
        return http.post('addToDo', {
            content: newToDo
        });
       
    },

    updateContent: (updateToDo) => {
        return http.put('updateContent/'+updateToDo.id, updateToDo);
       
    },

    updateToDo:(id) => {
        return http.post('checkedToDo/'+id);
    },

    deleteToDo:(id) => {
        return http.delete('deleteToDo/'+id);
    }
}