import Vue from 'vue';
import Router from 'vue-router';

import Home from '../views/Home.vue';
import ListDone from '../views/ListDone.vue';
import ListAll from '../views/ListAll.vue';

Vue.use(Router);

const routes = [
    {
        name: 'home',
        path: '/',
        component: Home,
    },
    {
        name: 'listdone',
        path: '/listdone',
        component: ListDone,
    },
    {
        name: 'listall',
        path: '/listall',
        component: ListAll,
    },
];

const router = new Router({routes});

export default router;